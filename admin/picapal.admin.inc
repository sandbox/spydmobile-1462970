<?php
/**
 * @file
 * Administration page callbacks for the picapal module.
 */

/**
 * Form builder. Configure annotations.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

function picapal_admin_settings() {

  $form['picapal_google_settings'] = array(
  '#type' => 'fieldset',
  '#title' => t('Google Account Settings (Username and Password)'),
  '#weight' => 5,
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  );
  $form['picapal_google_settings']['picapal_google_user_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Account Username:'),
    '#default_value' => variable_get('picapal_google_user_account', 'someone@gmail.com'),
    '#description' => t('enter the username you use to log into picasaweb.'),
  );
  $form['picapal_google_settings']['picapal_google_user_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Account Password:'),
     '#default_value' => variable_get('picapal_google_user_password',"12345678"),
    '#description' => t('The password to your picasaweb account.'),
  );




  // $form['#submit'][] = 'sparcs_admin_settings_submit';
  return system_settings_form($form);
}
